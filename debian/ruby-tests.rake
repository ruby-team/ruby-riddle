require 'rspec/core/rake_task'

ENV['CI'] = "1"
ENV['SPHINX_ENGINE'] = "sphinx"
ENV['SPHINX_VERSION'] = "2.2.11"

task :default do
if ENV['AUTOPKGTEST_TMP']
  sh "fakeroot ./debian/start_mysqld_and_run.sh #{RbConfig.ruby} -S rspec -f d"
else
  # do nothing
end
end
